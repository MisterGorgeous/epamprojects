package com.slabadniak.task5.entity;

public enum UserType {
    GUEST, USER, ADMINISTRATOR
}
