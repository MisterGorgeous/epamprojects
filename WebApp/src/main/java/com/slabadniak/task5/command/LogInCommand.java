package com.slabadniak.task5.command;

import com.slabadniak.task5.entity.Feedback;
import com.slabadniak.task5.entity.UserType;
import com.slabadniak.task5.entity.User;
import com.slabadniak.task5.exeption.CommandExeption;
import com.slabadniak.task5.exeption.ServiceExeption;
import com.slabadniak.task5.logic.Validation;
import com.slabadniak.task5.service.CheckUserService;
import com.slabadniak.task5.service.LogInService;
import com.slabadniak.task5.content.DataContext;
import com.slabadniak.task5.content.UserContent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


public class LogInCommand implements ICommand {
    private static final int UNIQUE = 0;
    private Feedback feedback;
    private static final String LOGIN = "Such login doesn't exist.";
    private static final String PASSWORD = "Incorrect password.";

    public LogInCommand() {
        this.feedback = new Feedback();
    }

    @Override
    public void execute(HttpServletRequest request) throws CommandExeption {

        String login = request.getParameter("login");
        String password = request.getParameter("pass");

        //if nothing is specified, don't close modal
        if(login == null || password == null){
            feedback = new Feedback();
            feedback.setMessage("");
            request.setAttribute(FEEDBACK, feedback);
            return;
        }

        //remove, if stay after previous query
        request.removeAttribute(FEEDBACK);

        feedback = Validation.checkPassword(password);
        if (feedback.isWritten()) {
            request.setAttribute(FEEDBACK, feedback);
            return;
        }
        feedback = Validation.checkLogin(login);
        if (feedback.isWritten()) {
            request.setAttribute(FEEDBACK, feedback);
            return;
        }

        User user = new User(login, password);
        user.hashPassword(); //MD5
        UserContent content;

        CheckUserService service1 = new CheckUserService();
        LogInService service = new LogInService();

        try {

            if (!service1.isLoginExist(user)) {
                feedback.setMessage(LOGIN);
                request.setAttribute(FEEDBACK, feedback);
                return;
            }

            if (!service1.checkPassword(user)) {
                feedback.setMessage(PASSWORD);
                request.setAttribute(FEEDBACK, feedback);
                return;
            }


            content = service.login(user);
        } catch (ServiceExeption e) {
            throw new CommandExeption("Service:", e);
        }


        setAtributes(content, request);
        if(!feedback.isWritten()){
            setForwardPage(request);
        }

    }

    private void setAtributes(UserContent content, HttpServletRequest request) {
        HttpSession session = request.getSession();
        List<User> users = content.get();

        if(users.isEmpty()){
            feedback.setMessage("You are banned.");
            request.setAttribute(FEEDBACK, feedback);
        }

        User user = users.get(UNIQUE);

        if (!user.isBanned()) {  //if doesn't banned
            session.setAttribute("user", user);
            if (user.isAdmin()) {
                session.setAttribute("userStatus", UserType.ADMINISTRATOR);
            } else {
                session.setAttribute("userStatus", UserType.USER);
            }
            //LOGGER.log(Level.DEBUG, "Loged in");
        } else {
            feedback.setMessage("You are banned.");
            request.setAttribute(FEEDBACK, feedback);
        }
    }
}